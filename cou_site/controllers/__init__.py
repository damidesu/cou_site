from flask import Blueprint, render_template, abort, redirect, url_for
from sqlalchemy.orm.exc import NoResultFound

from cou_site.controllers.team import developers
from cou_site.models.post import Post


core_pages = Blueprint("core_pages", __name__)


@core_pages.route("/")
def _index():
    latest_post = (
        Post.query.filter(Post.is_blog_post)
        .order_by(Post.created.desc())
        .limit(1)
        .one()
    )
    return render_template("index.html", latest_post=latest_post)


@core_pages.route("/about")
def _about():
    return render_template("about.html", team=developers)


@core_pages.route("/about/features")
def _features():
    return render_template("features.html")


@core_pages.route("/fine-print")
def _fine_print():
    return render_template("fine_print.html")


@core_pages.route("/help")
def _help():
    return render_template("help.html")


@core_pages.route("/login")
def _login():
    return render_template("login.html")


@core_pages.route("/edit/<post_id>")
def edit_post(post_id):
    if not str.isdigit(post_id):
        return abort(404)

    try:
        post = Post.query.filter(Post.entry_id == post_id).one()
    except NoResultFound:
        return abort(404)

    return render_template("edit.html", mode="post", entry=post, category=post.category)
