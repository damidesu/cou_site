from abc import abstractmethod
from collections import namedtuple
from datetime import datetime, timedelta
from urllib.request import urlopen
from urllib.error import HTTPError

from cou_site.controllers.util.encyclopedia import find_street_by_name


CategorizedData = namedtuple("CategorizedData", "by_id by_cat")
LocationsData = namedtuple("LocationsData", "hubs streets")


class CachedData:
    def __init__(self, max_age=None):
        self.max_age = max_age if max_age else timedelta(days=1)
        self._last_fetch = None
        self._data = None

    @abstractmethod
    def fetch(self):
        self._last_fetch = datetime.now()

    @property
    def is_current(self) -> bool:
        """Return whether the cached data is up-to-date."""
        return self._last_fetch and datetime.now() < self._last_fetch + self.max_age

    @property
    def data(self):
        """Return the data if it is current, otherwise refresh and return new data."""
        if not self._data or not self.is_current:
            self._data = self.fetch()

        return self._data


class CachedResource(CachedData):
    def __init__(self, url: str, max_age=None):
        super().__init__(max_age)
        self.url = url

    def fetch(self) -> str:
        """Get the data from the server and return it."""
        super().fetch()
        print("Fetching resource: " + self.url)

        if self.url.startswith("file://"):
            with open(self.url[len("file://") :]) as file:
                return file.read()
        else:
            try:
                with urlopen(self.url) as resource:
                    return resource.read()
            except HTTPError as e:
                print(f"Failed to fetch resource: {e}")


class CachedJSONResource(CachedResource):
    def fetch(self) -> dict:
        """Intercept data fetches and return JSON decoded to a dict for storage."""
        from json import loads

        return loads(super().fetch())


class CategorizedCachedJSONResource(CachedJSONResource):
    def __init__(self, url: str, dict_parser, categories):
        super().__init__(url)
        self.dict_parser = dict_parser
        self.categories = categories

    def fetch(self) -> CategorizedData:
        by_id = dict()
        by_cat = {category: set() for category in self.categories}
        resource = super().fetch()

        for data in resource.values() if type(resource) is dict else resource:
            obj = self.dict_parser(data)
            by_id[obj.obj_id] = obj
            by_cat[obj.category].add(obj)

        return CategorizedData(by_id=by_id, by_cat=by_cat)


class CachedEntitiesResource(CachedJSONResource):
    def fetch(self) -> dict:
        from cou_site.models.encyclopedia.entity import Entity

        entities = dict()

        for data in super().fetch():
            if data is None:
                continue

            entity = Entity.from_dict(data)
            entities[entity.obj_id] = entity

        return entities


class CachedLocationsResource(CachedJSONResource):
    def fetch(self) -> LocationsData:
        from cou_site.models.encyclopedia.hub import Hub
        from cou_site.models.encyclopedia.street import Street

        data = super().fetch()
        streets = dict()
        hubs = dict()

        for label, street_data in data.get("streets", dict()).items():
            street = Street.from_dict(label, street_data, self)
            streets[street.obj_id] = street

        for hub_id, hub_data in data.get("hubs", dict()).items():
            hub = Hub.from_dict(hub_id, hub_data, self)
            hubs[hub.obj_id] = hub

        return LocationsData(hubs=hubs, streets=streets)


class CachedAchievementsResource(CachedJSONResource):
    def fetch(self) -> CategorizedData:
        from cou_site.models.encyclopedia.achievement import Achievement, CATEGORIES

        achvs = dict()
        by_cat = {category.lower(): set() for category in CATEGORIES}

        for achv_data in super().fetch().values():
            achv = Achievement.from_dict(achv_data)
            achvs[achv.obj_id] = achv
            by_cat[achv.category.lower()].add(achv)

        return CategorizedData(by_id=achvs, by_cat=by_cat)


class CachedStreetsList(CachedData):
    def __init__(self, hub, locations: CachedLocationsResource):
        super().__init__(locations.max_age)
        self.hub = hub
        self.locations = locations

    def fetch(self) -> set:
        super().fetch()
        return {
            street
            for street in self.locations.data.streets.values()
            if street.category == self.hub.obj_id
        }


class CachedConnectingStreetsList(CachedData):
    def __init__(
        self, street, locations: CachedLocationsResource, graph: CachedJSONResource
    ):
        super().__init__(locations.max_age)
        self.street = street
        self.locations = locations
        self.graph = graph

    def fetch(self) -> set:
        super().fetch()
        connections = set()

        for edge in self.graph.data["edges"]:
            start = edge["start"]
            end = edge["end"]
            street = None

            if start == self.street.name:
                street = find_street_by_name(end, self.locations.data.streets)
            elif end == self.street.name:
                street = find_street_by_name(start, self.locations.data.streets)

            if street:
                connections.add(street)

        return connections
