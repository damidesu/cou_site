from datetime import datetime, timedelta

from flask import Blueprint, render_template
from sqlalchemy import and_

from cou_site.models.user import User


guides = Blueprint("guides", __name__, url_prefix="/guides")


@guides.route("/")
def _index():
    return render_template("guides.html", guides=list_guides())


def list_guides() -> list:
    """
    Return a list of guides who have logged in within the last year
    sorted with the most recently logged in first.
    """
    year_ago = datetime.utcnow() - timedelta(days=365)

    active_guides = User.query.filter(
        and_(
            User.elevation == "guide",
            User.last_login is not None,
            User.last_login > year_ago,
        )
    ).order_by(User.last_login.desc())

    return active_guides
