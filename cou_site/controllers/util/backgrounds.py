from os import path, listdir
from random import choice

from flask import current_app


backgrounds = list()


def rand_bg():
    global backgrounds

    if not backgrounds:
        bg_dir = path.join(current_app.static_folder, "img", "backgrounds")
        backgrounds = listdir(bg_dir)

    return choice(backgrounds)
