from collections import namedtuple

from flask import request


NavigationPage = namedtuple("NavigationPage", ["name", "url"])

pages = {
    "about": NavigationPage("About", "/about"),
    "blog": NavigationPage("Blog", "/blog"),
    "forums": NavigationPage("Forums", "/forums"),
    "profiles": NavigationPage("Profiles", "/players"),
    "encyclopedia": NavigationPage("Encyclopedia", "/encyclopedia"),
    "help": NavigationPage("Help", "/help"),
}


def is_active_page(navigation_page) -> bool:
    return request.path.startswith(navigation_page.url)
