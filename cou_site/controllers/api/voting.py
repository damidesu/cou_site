import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from cou_site.controllers.api import require_auth, require_reply_id, respond
from cou_site.models import db
from cou_site.models.reply import Reply

voting = Blueprint("voting", __name__, url_prefix="/api/vote")


@voting.route("/up", methods=["POST"])
@require_auth
@require_reply_id
def vote_up():
    reply_id = json.loads(request.data).get("reply_id")

    try:
        reply = Reply.query.filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return respond(ok=False, data="no such reply")

    if reply.user_id == session["user_id"]:
        return respond(ok=False, data="cannot upvote own reply")

    reply.vote_up(session["user_id"])
    db.session.commit()
    return respond()


@voting.route("/down", methods=["POST"])
@require_auth
@require_reply_id
def vote_down():
    reply_id = json.loads(request.data).get("reply_id")

    try:
        reply = Reply.query.filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return respond(ok=False, data="no such reply")

    if reply.user_id == session["user_id"]:
        return respond(ok=False, data="cannot downvote own reply")

    reply.vote_down(session["user_id"])
    db.session.commit()
    return respond()


@voting.route("/clear", methods=["POST"])
@require_auth
@require_reply_id
def vote_none():
    reply_id = json.loads(request.data).get("reply_id")

    try:
        reply = Reply.query.filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return respond(ok=False, data="no such reply")

    reply.clear_vote(session["user_id"])
    db.session.commit()
    return respond()
