from flask import Blueprint


migrate = Blueprint("migrate", __name__, url_prefix="/api/migrate")
