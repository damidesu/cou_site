from functools import wraps
import base64
import json
import re
from urllib.parse import quote
from urllib.request import urlopen

from flask import Blueprint, request, Response, session, jsonify
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound

from cou_site.config import game_server_uri
from cou_site.models.user import User

api = Blueprint("api", __name__, url_prefix="/api")


def respond(ok: bool = True, data: str = str()) -> Response:
    """
    :param ok: Whether the request was successful.
    :param data: Additional status information.
    :return: A Flask response.
    """
    return jsonify({"result": "OK" if ok else "ERROR", "data": data})


def require_auth(f):
    """Aborts the request with an error if the user is not logged in."""

    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Make sure the user is logged in
        if "auth" not in session:
            return respond(False, "not logged in"), 401

        # Make sure the user is a real user
        user_id = session["user_id"]
        try:
            User.query.filter(User.user_id == user_id).one()
        except NoResultFound:
            return respond(False, "user not found"), 401

        return f(*args, **kwargs)

    return decorated_function


def require_post_id(f):
    """
    Aborts the reponse with an error if the post id is not provided or
    is not an integer.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        post_id = json.loads(request.data).get("post_id")

        if not post_id:
            return respond(False, "missing post_id"), 400

        return f(*args, **kwargs)

    return decorated_function


def require_reply_id(f):
    """
    Aborts the response with an error if the reply id is not provided or
    is not an integer.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        reply_id = json.loads(request.data).get("reply_id")

        if not reply_id:
            return respond(False, "missing reply_id"), 400

        return f(*args, **kwargs)

    return decorated_function


def remove_script_tags(html: str) -> str:
    return re.sub(r"</?script[^>]*?>", str(), html)


@api.route("/avatar_head/<username>.png")
def avatar_head(username):
    username = quote(username)
    with urlopen(f"{game_server_uri}/trimImage?username={username}") as url:
        image_str = url.read()
    image = base64.b64decode(image_str)
    return Response(
        image,
        mimetype="image/png",
        headers={"Cache-Control": "public, max-age=604800, s-maxage=86400"},
    )


@api.route("/users")
def list_users():
    search = request.args.get("search", default=str()).lower()
    limit = int(request.args.get("limit", default="0"))

    usernames = [
        user.username
        for user in User.query.filter(func.lower(User.username).contains(search)).all()
    ]

    if 0 < limit < len(usernames):
        usernames = usernames[0:limit]

    return Response(
        "\n".join(usernames),
        mimetype="text/plain",
        headers={"Cache-Control": "public, max-age=3600, s-maxage=3600"},
    )
