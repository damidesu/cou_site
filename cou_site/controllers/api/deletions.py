import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from . import require_auth, require_reply_id, require_post_id, respond
from cou_site.models import db
from cou_site.models.reply import Reply
from cou_site.models.post import Post

deletions = Blueprint("deletions", __name__, url_prefix="/api/delete")


@deletions.route("/post", methods=["POST"])
@require_auth
@require_post_id
def delete_post():
    data = json.loads(request.data)
    post_id = data.get("post_id")

    try:
        post = Post.query.filter(Post.entry_id == post_id).one()
        url = post.category.url
    except NoResultFound:
        return respond(ok=False, data="Post not found. Was it already deleted?")

    if session["user_id"] != post.user_id:
        return respond(ok=False, data="cannot delete other user's post"), 403

    db.session.delete(post)
    db.session.commit()
    return respond(data=url)


@deletions.route("/reply", methods=["POST"])
@require_auth
@require_reply_id
def delete_reply():
    data = json.loads(request.data)
    reply_id = data.get("reply_id")

    try:
        reply = Reply.query.filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        return respond(ok=False, data="Reply not found. Was it already deleted?")

    if session["user_id"] != reply.user_id:
        return respond(ok=False, data="cannot delete other user's reply"), 403

    db.session.delete(reply)
    db.session.commit()
    return respond()
