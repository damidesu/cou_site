from os import getenv

# Keys
secret_key = getenv("SECRET_KEY")
rs_token = getenv("RS_TOKEN")

# Resources
db_uri = getenv("DB_URI")
game_server_host = getenv("GAME_SERVER_HOST")
game_server_uri = game_server_host + ":8181"

# Mail
mail_server = getenv("MAIL_SERVER")
mail_username = getenv("MAIL_USERNAME")
mail_password = getenv("MAIL_PASSWORD")

# App
port = int(getenv("PORT", 8583))
app_uri = getenv("APP_URI")
