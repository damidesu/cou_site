// Return a random property from an object
function randProp(obj) {
	let keys = Object.keys(obj);
	let index = keys.length * Math.random() << 0;
	return keys[index];
}

// 'snake_case' -> 'space case'
function formatName(snake) {
	return snake.replace(/_/g, ' ');
}

// 'snake_case' -> 'css-case'
function formatNameCss(snake) {
	return 'stat-' + snake.replace(/_/g, '-');
}

// Start flipping.
// initial: starting number value
// ms: interval between flips
function startCounter(initial, ms) {
	jQuery('#flipcounter').flipcountdown({
		tick: function(){
			return initial++;
		},
		size: 'lg',
		period: ms
	});
}

$(document).ready(function() {
	// Download data from server
	$.getJSON('https://server.childrenofur.com:8181/getGameStats', function(json) {
		// Stat name chosen at random
		let stat = randProp(json);

		// Last actual value of the stat
		let count = json[stat];

		// Fill in name
		$('#countText').html('&nbsp;' + formatName(stat) + ' and counting!');

		// Apply styles
		$('#countContainer').addClass(formatNameCss(stat));

		// Start flipping
		let interval = 2500; // 2.5 s

		if (stat === 'steps_taken') {
			interval = 200; // 1/5 s
		}

		if (stat === 'jumps' || stat === 'quoins_collected') {
			interval = 500; // 1/2 s
		}

		startCounter(count, interval);
	});
});
