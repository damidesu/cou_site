let username_input = $("#username-input");
let username_list = $("#username-list");
let busy = false;

username_input.on("input", function () {
    if (busy) {
        return;
    }

    busy = true;

    $.get("/api/users?limit=5&search=" + username_input.val(), function (data) {
        username_list.empty();

        let usernames = data.split("\n");
        usernames.forEach(function (value) {
            let item = document.createElement("option");
            item.value = value;
            username_list.append(item);
        });

        busy = false;
    });
});

function goToPlayer() {
    window.location.href = "/players/" + username_input.val();
}
