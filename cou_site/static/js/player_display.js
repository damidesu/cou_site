const server = "https://server.childrenofur.com:8181";
const scaleFactor = 1.3;

function clearChildNodes(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

$(function () {
    let username = $('[name="X-CoU-Player-Username"]').attr("content");

    // Connect to the server to download the avatar image
    $.getJSON(server + "/getSpritesheets?username=" + username, function (data) {
        let image = new Image();

        // Size the image
        image.onload = function () {
            // Create the canvas
            let canvas = document.createElement("canvas");
            canvas.width = Math.round(image.naturalWidth / 15 * scaleFactor);
            canvas.height = Math.round(image.naturalHeight * scaleFactor);

            // Put the image on the canvas
            let ctx = canvas.getContext("2d");
            ctx.scale(scaleFactor, scaleFactor);
            ctx.drawImage(image, 0, 0,);

            // Output to page
            let parent = $("#profile-player");
            clearChildNodes(parent);
            parent.append(canvas);
        };

        image.src = data["base"];
    });
});

$(function () {
    // Handle username color submissions
    let colorSubmit = $("#color-submit");
    colorSubmit.click(function () {
        colorSubmit.button("loading");
        $.ajax({
            type: "POST",
            url: "/api/account/color",
            data: $('.color-swatch[current="true"]').attr("data-color"),
            success: function () {
                colorSubmit.button("complete");
            },
            error: function (xhr, status, error) {
                colorSubmit.button("reset");
                alert("Could not set username color: " + error);
            }
        });
    });

    let swatches = $(".color-swatch");
    swatches.click(function () {
        colorSubmit.button("reset");
        swatches.removeAttr("current");
        $(this).attr("current", "true");
    });
});

$(function () {
    // Handle bio submissions
    let bioSubmit = $("#savebio");
    bioSubmit.click(function () {
        bioSubmit.button("loading");

        $.ajax({
            type: "POST",
            url: "/api/account/bio",
            data: tinyMCE.activeEditor.getContent().trim(),
            success: function () {
                bioSubmit.button("complete");
                window.location.reload();
            },
            error: function (xhr, status, error) {
                bioSubmit.button("reset");
                alert("Could not set bio: " + error);
            }
        });
    });
});
