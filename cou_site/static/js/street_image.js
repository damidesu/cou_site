class StreetImage {
    constructor(element) {
        // Elements
        this.host = element;
        this.button = element.querySelector("button");
        this.buttonIcon = this.button.querySelector("i");

        // Parameters
        this.mainImageUrl = this.host.dataset.mainImageUrl;
        this.mainImageWidth = parseInt(this.host.dataset.mainImageWidth);
        this.mainImageHeight = parseInt(this.host.dataset.mainImageHeight);
        this.loadingImageUrl = this.host.dataset.loadingImageUrl;
        this.loadingImageWidth = parseInt(this.host.dataset.loadingImageWidth);
        this.loadingImageHeight = parseInt(this.host.dataset.loadingImageHeight);

        // Event handler
        let object = this;
        this.button.addEventListener("click", function () {
           object.toggle();
        });

        // Start in loading image view
        this.collapse();
    }

    collapse() {
        this.open = false;

        // Change displayed image
        this.host.style.backgroundImage = "url(" + this.loadingImageUrl + ")";

        // Update zoom icon
        this.buttonIcon.classList.add("fa-expand");
        this.buttonIcon.classList.remove("fa-compress");

        // Resize
        this.host.classList.remove("open");
        this.host.classList.remove("active");
        this.scale();
    }

    expand() {
        this.open = true;

        // Change displayed image
        this.host.style.backgroundImage = "url(" + this.mainImageUrl + ")";

        // Update zoom icon
        this.buttonIcon.classList.add("fa-compress");
        this.buttonIcon.classList.remove("fa-expand");

        // Resize
        this.host.classList.add("open");
        this.host.classList.add("active");
        this.scale();
    }

    toggle() {
        if (this.open) {
            this.collapse();
        } else {
            this.expand();
        }
    }

    // Update the sizing for the current window
    scale() {
        let scaleFactor, scaledHeight;

        if (this.open) {
            scaleFactor = this.host.clientWidth / this.mainImageWidth;
            scaledHeight = this.mainImageHeight * scaleFactor;
        } else {
            scaleFactor = this.host.clientWidth / this.loadingImageWidth;
            scaledHeight = this.loadingImageHeight * scaleFactor;
        }

        this.host.style.height = scaledHeight + "px";
    }
}
