import json

from sqlalchemy import Column, String, Boolean
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from cou_site.controllers.util.navigation import pages
from . import db
from .category import Category, BLOG_CATEGORIES
from .entry import Entry
from .reply import Reply

PAGE = "post"


class Post(db.Model, Entry):
    __tablename__ = "posts"

    title = Column(String)
    category_id = Column("category", String, nullable=False)
    _watchers = Column("watchers", String, nullable=False, default="[]")
    is_draft = Column(Boolean, nullable=False, default=False)

    def __init__(self, category_id, title, content, user_id):
        self.category_id = category_id
        self.title = title
        self.content = content
        self.user_id = user_id

    @property
    def watchers(self) -> set:
        return set(json.loads(self._watchers))

    @property
    def num_watchers(self) -> int:
        return len(self.watchers)

    def add_watcher(self, user_id):
        self._watchers = json.dumps(list(self.watchers | {user_id}))

    def remove_watcher(self, user_id):
        self._watchers = json.dumps(list(self.watchers - {user_id}))

    @property
    def category(self):
        return Category.find(self.category_id)

    @property
    def url(self) -> str:
        page = "blog" if self.is_blog_post else "forums"
        return f"{pages[page].url}/{PAGE}/{self.entry_id}"

    @declared_attr
    def replies(self):
        return relationship(Reply, lazy="joined", cascade="delete")

    @property
    def num_replies(self) -> int:
        return len(self.replies)

    @property
    def edit_url(self):
        if self.is_blog_post:
            return f"/blog/edit?post={self.entry_id}"
        else:
            return f"/forums/edit/post?id={self.entry_id}"

    @hybrid_property
    def is_blog_post(self) -> bool:
        return self.category.is_blog_category

    @is_blog_post.expression
    def is_blog_post(self) -> bool:
        return self.category_id.in_({c.cat_id for c in BLOG_CATEGORIES})
