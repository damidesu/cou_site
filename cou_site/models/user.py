from sqlalchemy import Column, DateTime, Integer, String, Boolean, JSON
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from . import db
from .metabolics import Metabolics
from cou_site.controllers.util.navigation import pages

PROFILE_PAGE = pages["profiles"].url


class User(db.Model):
    @staticmethod
    def make_avatar_url(username: str) -> str:
        return f"/api/avatar_head/{username}.png"

    __tablename__ = "users"

    user_id = Column("id", Integer, primary_key=True)
    username = Column(String)
    email = Column(String)
    bio = Column(String)
    registration_date = Column(DateTime)
    username_color = Column(String)
    chat_disabled = Column(Boolean)
    achievements = Column(JSON)
    last_login = Column(DateTime)
    elevation = Column(String)
    custom_avatar = Column(String)
    friends = Column(JSON)

    @declared_attr
    def metabolics(self):
        return relationship(Metabolics, lazy="joined", uselist=False)

    @property
    def profile_url(self):
        return f"{PROFILE_PAGE}/{self.username}"

    @property
    def avatar_url(self):
        # Server handles custom_avatar overrides
        return User.make_avatar_url(self.username)

    @property
    def is_guide(self):
        return self.elevation == "guide"

    @property
    def is_dev(self):
        return self.elevation == "dev"

    def __str__(self):
        return f"User {self.user_id} ({self.username})"
