from cou_site.models.encyclopedia.game_object import GameObject


PATH = "giants"
_IMAGES = "https://childrenofur.com/assets/giants"


class Giant(GameObject):
    def __init__(
        self,
        name: str,
        domain: str,
        characteristics: list,
        description: str,
        adherents: str,
    ):
        super().__init__(
            obj_id=name.lower(),
            name=name,
            category=None,
            icon_url=f"{_IMAGES}/{name}.png",
        )
        self.domain = domain
        self.characteristics = characteristics
        self.description = description
        self.adherents = adherents

    @property
    def path(self) -> str:
        return PATH


GIANTS = {
    "alph": Giant(
        name="Alph",
        domain="Creation",
        characteristics=["Playful", "Inconsistent", "Unreliable"],
        description=(
            'Alph is the giant responsible for creating "things". And also '
            '"stuff". Approaching everything with the question "What IF?", '
            "Alph is never happier than when the answer results in a complex "
            "wadjamacallit or a satisfyingly big boom."
        ),
        adherents="Alphas",
    ),
    "cosma": Giant(
        name="Cosma",
        domain="Sky & Meditation",
        characteristics=["Levitous", "Aimless"],
        description=(
            "As flighty and aimless as anything can be (if that thing is "
            "also giant-sized), Cosma is the Giant in charge of imagining up "
            "all things airborne. From the heaviest gas to cling to a cavern "
            "floor to the tiniest fart to escape from a butterfly, Cosma is "
            "the source of it all."
        ),
        adherents="Cosmapolitans",
    ),
    "friendly": Giant(
        name="Friendly",
        domain="Night & Social Life",
        characteristics=["Empathetic", "Meddlesome"],
        description=(
            "Friendly is the overseer of darkness, nocturnal things, "
            "party-planning, of social activities and their most common "
            "lubricant, booze. Friendly by name, friendly by nature: "
            "unless you neglect to buy a round."
        ),
        adherents="Friends",
    ),
    "grendaline": Giant(
        name="Grendaline",
        domain="Water",
        characteristics=["Loyal", "Fierce"],
        description=(
            "Grendaline, raised in a swamp, has an imagination as "
            "free-flowing as water. This makes sense, as water is "
            "what she spends most of her time imagining. If it sprinkles, "
            "drips, flows or gushes, Grendaline is your giant."
        ),
        adherents="Grendalinians",
    ),
    "humbaba": Giant(
        name="Humbaba",
        domain="Walking Creatures",
        characteristics=["Gregarious", "Belligerent"],
        description=(
            "Humbaba is the giant ruling over all creatures that walk, "
            "crawl, slither or sashay over Ur. One with the animals, "
            "Humbaba walks on all fours to be closer to them, and insists "
            "on them calling her by her first name. Which is kind of "
            "pointless, since she only has one name."
        ),
        adherents="Humbabans",
    ),
    "lem": Giant(
        name="Lem",
        domain="Exploration",
        characteristics=["Open", "Unavailable", "Evasive"],
        description=(
            "Lem, the wanderer giant. Responsible for travel, directions, "
            "and knowledge. What Lem doesn’t know is not worth knowing. "
            "Also, what Lem doesn’t unknow is not worth unknowing. "
            "(That’s Lem’s favourite joke) (Lem doesn’t know many jokes)."
        ),
        adherents="Lemmings",
    ),
    "mab": Giant(
        name="Mab",
        domain="Harvesting",
        characteristics=["Industrious", "Greedy"],
        description=(
            "When the harvest needs bringing in, the crops need counting, "
            "and the job needs doing right, Mab, Giant of Soil and Harvests, "
            "first to lie down and start imagining Ur into being is the "
            "giant to look to."
        ),
        adherents="Mabbites",
    ),
    "pot": Giant(
        name="Pot",
        domain="Prosperity",
        characteristics=["Generous", "Gluttonous", "Impatient"],
        description=(
            "Round of belly and capacious of stomach Pot is the Giant of Prosperity, "
            "with dominion over anything edible, cookable, munchable or nibbleworthy. "
            "Pot himself is not munchable. Do not attempt to munch any giants."
        ),
        adherents="Potians",
    ),
    "spriggan": Giant(
        name="Spriggan",
        domain="Trees",
        characteristics=["Persistent", "Evergreen", "Rigid"],
        description=(
            "Before Spriggan, no giant had ever imagined a tree. "
            "After Spriggan, no giant ever needed to, because he had "
            "already imagined them all. Steadfast, persistent, and "
            "somewhat rigid, Spriggan is the slumbering Giant of all "
            'Trees and Plants. Or "Trants".'
        ),
        adherents="Spriggots",
    ),
    "tii": Giant(
        name="Tii",
        domain="Odd & Even Numbers",
        characteristics=["Cold", "Reckoning"],
        description=(
            "The Giant with power over all numbers. Odd or even, "
            "prime or not-prime, cubed or rooted, Tii keeps a cold, "
            "watchful eye over Ur. Tii sees all, knows all, calculates "
            "all. Never underestimate Tii. Tii has already correctly "
            "estimated you."
        ),
        adherents="Ti'ites",
    ),
    "zille": Giant(
        name="Zille",
        domain="Mountains",
        characteristics=["Friendly", "Neurotic"],
        description=(
            "Zille is the giant with dominion over the mountains. "
            "All rocks, caverns, hillocks, pingos, buttes and drumlins "
            "thank Zille for their existence. Zille, busy imagining up "
            "new flavours of gravel, acknowledges their thanks."
        ),
        adherents="Zillots",
    ),
}
