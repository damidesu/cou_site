from abc import abstractmethod
from typing import Union


class GameObject:
    def __init__(
        self, obj_id: str, name: str, category: Union[str, None], icon_url: str
    ):
        self.obj_id = obj_id
        self.name = name
        self.category = category
        self.icon_url = icon_url

    @property
    @abstractmethod
    def path(self) -> str:
        pass

    @property
    def url(self) -> str:
        return f"/encyclopedia/{self.path}/{self.obj_id}"
