from .game_object import GameObject


PATH = "items"

CATEGORIES = [
    "Advanced Resources",
    "Alchemical Compounds & Powders",
    "Basic Resources",
    "Collectibles",
    "Croppery & Gardening Supplies",
    "Drink",
    "Emblems & Icons",
    "Food",
    "Gasses & Bubbles",
    "Herbalism Supplies",
    "Herdkeeping Supplies",
    "Licenses & Permits",
    "Machines & Fuel",
    "Keys",
    "Other",
    "Quest Items",
    "Seeds",
    "Sno Cones",
    "Spices",
    "Storage",
    "Tinctures & Potions",
    "Tools",
    "Toys",
]


class Item(GameObject):
    @staticmethod
    def from_dict(item: dict):
        return Item(
            obj_id=item["itemType"],
            name=item["name"],
            category=item["category"],
            icon_url=item.get("iconUrl", str()),
            sprite_url=item.get("sprite_url", str()),
            icon_num=item.get("icon_num", 4),
            description=item.get("description", str()),
            price=item.get("price", 0),
            stacks_to=item.get("stacksTo", 1),
            sub_slots=item.get("subSlots", 0),
            is_container=item.get("isContainer", False),
            consume_values=item.get("consumeValues", dict()),
        )

    def __init__(
        self,
        obj_id: str,
        name: str,
        category: str,
        icon_url: str,
        sprite_url: str,
        icon_num: int,
        description: str,
        price: int,
        stacks_to: int,
        sub_slots: int,
        is_container: bool,
        consume_values: dict,
    ):
        super().__init__(obj_id, name, category, icon_url)
        self.sprite_url = sprite_url
        self.icon_num = icon_num
        self.description = description
        self.price = price
        self.stacks_to = stacks_to
        self.sub_slots = sub_slots
        self.is_container = is_container
        self.consume_values = consume_values

    @property
    def path(self) -> str:
        return PATH
