from sqlalchemy import Column, Integer, String, Float, JSON, ForeignKey
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from . import QUOIN_LIMIT, QUOIN_MULTIPLIER_LIMIT, LEVEL_IMG, db


class Metabolics(db.Model):
    __tablename__ = "metabolics"

    metabolics_id = Column("id", Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"))

    @declared_attr
    def user(self):
        return relationship("User", lazy="joined")

    # Stats
    currants = Column(Integer, nullable=False, default=500)
    energy = Column(Integer, nullable=False, default=100)
    max_energy = Column(Integer, nullable=False, default=100)
    mood = Column(Integer, nullable=False, default=100)
    max_mood = Column(Integer, nullable=False, default=100)

    @property
    def energy_percent(self) -> float:
        return self.energy / self.max_energy * 100

    @property
    def mood_percent(self) -> float:
        return self.mood / self.max_mood * 100

    # iMG
    img = Column(Integer, nullable=False, default=0)
    lifetime_img = Column(Integer, nullable=False, default=0)

    @property
    def level(self) -> int:
        for level in LEVEL_IMG.keys():
            if self.img < LEVEL_IMG[level]:
                return level - 1

        return LEVEL_IMG.keys()[-1]

    # Location
    current_street = Column(String)
    current_street_x = Column(Float)
    current_street_y = Column(Float)
    location_history = Column(JSON)
    last_street = Column(String)
    undead_street = Column(String)

    @property
    def locations_visited(self) -> int:
        return len(self.location_history)

    # Quoins
    quoins_collected = Column(Integer, nullable=False, default=0)
    quoin_multiplier = Column(Float, nullable=False, default=1)

    @property
    def quoins_collected_percent(self) -> float:
        return self.quoins_collected / QUOIN_LIMIT * 100

    @property
    def quoin_multiplier_percent(self) -> float:
        return self.quoin_multiplier / QUOIN_MULTIPLIER_LIMIT * 100

    # Advancements
    skills = Column("skills_json", JSON, nullable=False, default=dict)
    buffs = Column("buffs_json", JSON, nullable=False, default=dict)

    # Current favor
    alphfavor = Column(Integer, nullable=False, default=0)
    cosmafavor = Column(Integer, nullable=False, default=0)
    friendlyfavor = Column(Integer, nullable=False, default=0)
    grendalinefavor = Column(Integer, nullable=False, default=0)
    humbabafavor = Column(Integer, nullable=False, default=0)
    lemfavor = Column(Integer, nullable=False, default=0)
    mabfavor = Column(Integer, nullable=False, default=0)
    potfavor = Column(Integer, nullable=False, default=0)
    sprigganfavor = Column(Integer, nullable=False, default=0)
    tiifavor = Column(Integer, nullable=False, default=0)
    zillefavor = Column(Integer, nullable=False, default=0)

    # Maximum favor
    alphfavor_max = Column(Integer, nullable=False, default=0)
    cosmafavor_max = Column(Integer, nullable=False, default=0)
    friendlyfavor_max = Column(Integer, nullable=False, default=0)
    grendalinefavor_max = Column(Integer, nullable=False, default=0)
    humbabafavor_max = Column(Integer, nullable=False, default=0)
    lemfavor_max = Column(Integer, nullable=False, default=0)
    mabfavor_max = Column(Integer, nullable=False, default=0)
    potfavor_max = Column(Integer, nullable=False, default=0)
    sprigganfavor_max = Column(Integer, nullable=False, default=0)
    tiifavor_max = Column(Integer, nullable=False, default=0)
    zillefavor_max = Column(Integer, nullable=False, default=0)

    @property
    def alphfavor_percent(self) -> float:
        return self.alphfavor / self.alphfavor_max * 100

    @property
    def cosmafavor_percent(self) -> float:
        return self.cosmafavor / self.cosmafavor_max * 100

    @property
    def friendlyfavor_percent(self) -> float:
        return self.friendlyfavor / self.friendlyfavor_max * 100

    @property
    def grendalinefavor_percent(self) -> float:
        return self.grendalinefavor / self.grendalinefavor_max * 100

    @property
    def humbabafavor_percent(self) -> float:
        return self.humbabafavor / self.humbabafavor_max * 100

    @property
    def lemfavor_percent(self) -> float:
        return self.lemfavor / self.lemfavor_max * 100

    @property
    def mabfavor_percent(self) -> float:
        return self.mabfavor / self.mabfavor_max * 100

    @property
    def potfavor_percent(self) -> float:
        return self.potfavor / self.potfavor_max * 100

    @property
    def sprigganfavor_percent(self) -> float:
        return self.sprigganfavor / self.sprigganfavor_max * 100

    @property
    def tiifavor_percent(self) -> float:
        return self.tiifavor / self.tiifavor_max * 100

    @property
    def zillefavor_percent(self) -> float:
        return self.zillefavor / self.zillefavor_max * 100

    def __str__(self):
        return f"Metabolics {self.metabolics_id} for user {self.user_id}"
